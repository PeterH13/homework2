#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    //build a cube of objects with all three axis
    for(int x = 0; x< 10; x++){
        for(int y = 0; y< 10; y++){
            for(int z = 0; z< 10; z++){
                int currOrb = ((x*10)+y)*10+z; //calculate the index for the orb
                orbs[currOrb].setPosition(x*20, y*20, z*20); //position the orb
                orbs[currOrb].setRadius(5); //set the size of the orbs
            }
        }
    }
    for(int x2 = 0; x2< 10; x2++){
        for(int y2 = 0; y2< 10; y2++){
            for(int z2 = 0; z2< 10; z2++){
                int currOrb = ((x2*10)+y2)*10+z2; //calculate the index for the orb
                orbsB[currOrb].setPosition(x2*20, y2*20, z2*20); //position the orb
                orbsB[currOrb].setRadius(1); //set the size of the orbs
            }
        }
    }
    //Camera Position
    cam.setPosition(100, 100, 300);
    
    //setup a warm light
    light1.setDiffuseColor(ofColor(255, 133, 133, 200));
    light1.setPosition(-400, 100, 500);
    light1.enable();
    
    //setup a cool light
    light2.setDiffuseColor(ofColor(168, 222, 255, 200));
    light2.setPosition(400, 100, 500);
    light2.enable();
}

//--------------------------------------------------------------
void ofApp::update(){
    noiseTime+=0.03; //move our timer forward
    //use our 3 axis grid again so we have the positions of each orb
    for(int x = 0; x< 10; x++){
        for(int y = 0; y< 10; y++){
            for(int z = 0; z< 10; z++){
                int currOrb = ((x*10)+y)*10+z; //calculate the index again
                float myNoise = 25.0*ofSignedNoise(x/8.0, y/8.0-noiseTime, z/8.0, noiseTime); //figure out the noise at those coordinates
                if(myNoise<0)myNoise = 0; //do not use the negative noise
                orbs[currOrb].setRadius(myNoise); //change the size of the orb with the noise
            }
        }
    }
    
    noiseTimeB+=0.08; //move our timer forward
    
    for(int x2 = 0; x2< 10; x2++){
        for(int y2 = 0; y2< 10; y2++){
            for(int z2 = 0; z2< 10; z2++){
                int currOrb = ((x2*10)+y2)*10+z2; //calculate the index again
                float myNoiseB = 25.0*ofSignedNoise(x2/8.0, y2/8.0-noiseTimeB, z2/8.0, noiseTimeB); //figure out the noise at those coordinates
                if(myNoiseB<0)myNoiseB = 0; //do not use the negative noise
                orbsB[currOrb].setRadius(myNoiseB/2); //change the size of the orb with the noise
            }
        }
    }
}

//--------------------------------------------------------------
void ofApp::draw(){
    
    ofEnableDepthTest(); //sort the drawing so that the things closest to the camera is in front
    cam.begin();
    ofRotateDeg(45, 0, 1, 0); //rotate the cube 45 degrees on the y-axis
    
    for(int i = 0; i<1000; i++){
        ofSetColor((orbs[i].getRadius()*6.0), 200);//set the color so they are brighter as they are bigger
        orbs[i].draw();//draw all the orbs
    }
    for(int i = 0; i<1000; i++){
        ofSetColor((orbsB[i].getRadius()*60.0), 200);//set the color so they are brighter as they are bigger
        orbsB[i].draw();//draw all the orbs
    }
    cam.end();
    ofDisableDepthTest();
    
    cam.draw(); //draw whatever the camera saw
}


