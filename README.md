Title: Colorful Bubbles

Screenshot:
![](https://user-images.githubusercontent.com/624713/73393432-ddb04380-4298-11ea-9dc3-f9f4e01ef977.png)

Description: For this assignment I used an example of 100 spheres drawn in 3D space with noise determining the size of the spheres. 
I added new for loops and a new noise variable to draw another set of spheres in the same space but in white. I like how the two 
sets of spheres interact because of the different noise and color. I also added a blue and pink light, but was unable to get the lights
as bright as I would have liked.